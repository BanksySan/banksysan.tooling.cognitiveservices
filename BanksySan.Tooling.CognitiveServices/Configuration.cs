﻿namespace BanksySan.Tooling.CognitiveServices
{
    using ApplicationConfiguration;
    using Newtonsoft.Json;

    internal class Configuration
    {
        [JsonProperty("face-api-details")] public FaceApiInfo[] FaceApiInfos { get; set; }
    }
}