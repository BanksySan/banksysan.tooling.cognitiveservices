﻿namespace BanksySan.Tooling.CognitiveServices
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.ProjectOxford.Face;
    using static System.Console;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            Task.WaitAll(MainAsync(args));
        }

        private static async Task MainAsync(string[] args)
        {
            var config = ConfigurationReader.GetConfiguration();
            var faceApiInfo = Ui.DisplaySelectConfiguration(config);
            var client = new FaceServiceClient(faceApiInfo.Key1, faceApiInfo.Url);
            var application = new Application(client);
            while (true)
            {
                try
                {
                    await Ui.DoActions(application.Actions);
                }
                catch (FaceAPIException e)
                {
                    WriteLine("ERROR");
                    WriteLine("Exception:");
                    WriteLine($"HTTP Details:  {e.HttpStatus} {e.ErrorCode} {e.ErrorMessage}");
                    WriteLine($"Message:  {e.Message}");

                    WriteLine($"InnerException: {(e.InnerException?.Message ?? "null")}");
                }
                catch (Exception e)
                {
                    WriteLine("ERROR");
                    WriteLine("Exception:");
                    WriteLine(e);
                }
            }
        }
    }
}