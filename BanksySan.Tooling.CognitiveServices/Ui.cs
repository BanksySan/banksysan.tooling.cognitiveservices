﻿namespace BanksySan.Tooling.CognitiveServices
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using ApplicationConfiguration;

    internal static class Ui
    {
        public static async Task DoActions((string Name, Func<Task> Action)[] actions)
        {
            var actionsArray = actions.ToArray();

            Console.WriteLine($"What next?");
            for (var i = 0; i < actionsArray.Length; i++)
            {
                var action = actionsArray[i];
                Console.WriteLine($"{i,-3} {action.Name}");
            }

            var selection = GetSelection(i => !(i < 0) && i < actionsArray.Length);
            await actionsArray[selection].Action();
        }

        public static FaceApiInfo DisplaySelectConfiguration(Configuration config)
        {
            Console.WriteLine("Select the API to use:");
            var faceApiInfos = config.FaceApiInfos;

            for (var i = 0; i < faceApiInfos.Length; i++)
            {
                Console.WriteLine($"{i,-3} URL: {faceApiInfos[i].Url}");
                Console.WriteLine($"    Key 1:  {faceApiInfos[i].Key1}");
                Console.WriteLine($"    Key 2:  {faceApiInfos[i].Key2}");
            }

            var userSelection = GetSelection(x => !(x < 0) &&
                                                  x < config.FaceApiInfos.Length);

            var faceApiInfo = config.FaceApiInfos[userSelection];
            Console.WriteLine($"Using {faceApiInfo.Url} (\"{faceApiInfo.Key1}\".");
            return faceApiInfo;
        }

        private static int GetSelection(Func<int, bool> isValid)
        {
            var selection = Console.ReadLine();
            if (int.TryParse(selection, out var userSelection) && isValid(userSelection))
            {
                return userSelection;
            }

            Console.WriteLine("No.  Try again.");
            return GetSelection(isValid);
        }
    }
}