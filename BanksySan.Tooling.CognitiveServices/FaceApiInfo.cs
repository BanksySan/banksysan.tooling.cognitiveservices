﻿using Newtonsoft.Json;

namespace BanksySan.Tooling.CognitiveServices.ApplicationConfiguration
{
    internal class FaceApiInfo
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("key-1")]
        public string Key1 { get; set; }
        [JsonProperty("key-2")]
        public string Key2 { get; set; }
    }
}