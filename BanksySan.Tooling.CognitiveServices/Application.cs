﻿namespace BanksySan.Tooling.CognitiveServices
{
    using System;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Microsoft.ProjectOxford.Face;
    using Newtonsoft.Json;
    using static System.Console;

    internal class Application
    {
        private const string SAFE_CHARACTERS_REGEX_STRING = @"^[a-zA-Z0-9-]+$";
        private static readonly Regex SAFE_CHARACTERS_REGEX = new Regex(SAFE_CHARACTERS_REGEX_STRING);

        private readonly IFaceServiceClient _client;

        internal Application(IFaceServiceClient faceServiceClient)
        {
            _client = faceServiceClient ?? throw new ArgumentNullException(nameof(faceServiceClient));
        }

        public (string Name, Func<Task> Action)[] Actions => new (string Name, Func<Task> Action)[]
        {
            (nameof(DetectFaces), DetectFaces),
            (nameof(ListGroups), ListGroups),
            (nameof(CreatePersonGroup), CreatePersonGroup),
            (nameof(CreatePerson), CreatePerson),
            (nameof(AddPersonFace), AddPersonFace),
            (nameof(GetPersonGroupPeople), GetPersonGroupPeople)
        };

        private async Task DetectFaces()
        {
            WriteLine("Enter file path: ");
            var filePath = ReadLine() ?? throw new InvalidOperationException("We need a path");
            var fileInfo = new FileInfo(filePath);
            using (var fileStream = fileInfo.OpenRead())
            {
                var allFaceAttributes = new[]
                {
                    FaceAttributeType.Age, FaceAttributeType.FacialHair, FaceAttributeType.Gender,
                    FaceAttributeType.Glasses, FaceAttributeType.HeadPose, FaceAttributeType.Smile
                };

                var faces = await _client.DetectAsync(fileStream, true, true, allFaceAttributes);

                foreach (var face in faces)
                {
                    WriteLine($"Face ID:  {face.FaceId}");
                    WriteLine($"Attributes:  {JsonConvert.SerializeObject(face.FaceAttributes, Formatting.Indented)}");
                    WriteLine($"Landmarks:  {JsonConvert.SerializeObject(face.FaceLandmarks, Formatting.Indented)}");
                }
            }
        }

        private async Task ListGroups()
        {
            var faceLists = await _client.ListFaceListsAsync();
            var personGroups = await _client.ListPersonGroupsAsync();
            if (faceLists.Length == 0)
            {
                WriteLine("No face lists found");
            }
            else
            {
                foreach (var faceList in faceLists)
                {
                    WriteLine($"Face list ID: {faceList.FaceListId}");
                    WriteLine($"Name: {faceList.Name}");
                    WriteLine($"User data: {faceList.UserData}");
                }
            }

            if (personGroups.Length == 0)
            {
                WriteLine("No person groups");
            }
            else
            {
                foreach (var personGroup in personGroups)
                {
                    WriteLine($"Person group ID:  {personGroup.PersonGroupId}");
                    WriteLine($"Name:  {personGroup.Name}");
                    WriteLine($"User data: {personGroup.UserData}");
                }
            }
        }

        private async Task CreatePersonGroup()
        {

            WriteLine($"Enter name ({SAFE_CHARACTERS_REGEX_STRING}):");
            var name = ReadUserInput();
            var id = Guid.NewGuid().ToString("D");
            WriteLine("Generating group.");
            WriteLine($"ID: {id}");
            WriteLine($"Name: {name}");
            await _client.CreatePersonGroupAsync(id, name);
            WriteLine("Successfully created group.");
        }

        private async Task CreatePerson()
        {
            WriteLine("Enter person name: ");
            var name = ReadUserInput();
            WriteLine("Enter group id");
            var groupId = ReadUserInput();
            var personResult = await _client.CreatePersonAsync(groupId, name);
            WriteLine($"Created {personResult.PersonId} in {groupId}");
        }

        private async Task AddPersonFace()
        {
            WriteLine("Person group ID:");
            var personGroupId = ReadUserInput();
            WriteLine("Person ID:");
            var personId = ReadUserInput();
            WriteLine($"File path:");
            var filePath = ReadLine();
            using (var fileStream = File.OpenRead(filePath))
            {
                var addPersonResult = await _client.AddPersonFaceAsync(personGroupId, Guid.Parse(personId), fileStream);
                WriteLine($"Persisted face ID: {addPersonResult.PersistedFaceId}");
            }
        }

        private async Task GetPersonGroupPeople()
        {
            WriteLine("Person group ID");
            var personGroupId = ReadUserInput();
            var persons = await _client.GetPersonsAsync(personGroupId);
            foreach (var person in persons)
            {
                WriteLine($"ID: {person.PersonId:D}");
                WriteLine($"Name: {person.Name}");
                WriteLine($"Face IDs: {string.Join(", ", person.PersistedFaceIds)}");
            }
        }
        private string ReadUserInput()
        {
            var input = ReadLine();
            while (!SAFE_CHARACTERS_REGEX.IsMatch(input))
            {
                WriteLine($"Nope.  Try again.  {SAFE_CHARACTERS_REGEX}");
            }

            return input;
        }
    }
}