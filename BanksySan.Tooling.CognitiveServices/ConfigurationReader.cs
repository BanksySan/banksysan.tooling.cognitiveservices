﻿namespace BanksySan.Tooling.CognitiveServices
{
    using System.IO;
    using Newtonsoft.Json;

    internal static class ConfigurationReader
    {
        public static Configuration GetConfiguration()
        {
            using (var configurationTextReader = File.OpenText("configuration.json"))
            {
                return JsonConvert.DeserializeObject<Configuration>(configurationTextReader.ReadToEnd());
            }
        }
    }
}